# Goals of the Foundation
The Haskell Foundation (HF) is an independent, non-profit organization dedicated
to broadening the adoption of Haskell, by supporting its ecosystem of tools,
libraries, education, and research.

# The values of the Foundation
How we pursue the goals of HF is just as important as what the goals are. The actions
of the Foundation and its Board will be guided by these core principles:

* **Open source**. Haskell is an open source community and HF will embrace the
  open-source ethos wholeheartedly. HF may develop, or sponsor the development
  of tools and infrastructure, but it will all be open source.

* **Empowering the community**. A major goal of HF is to boost, celebrate, and
  coordinate the contributions and leadership of volunteers, not to supplant or
  replace them.

* **Open, friendly, and diverse**. For many of us Haskell is more a way of life
  than a programming language. We strive to make sure that all are welcome, that
  all can contribute, and that it can all be done in a way that empowers our
  community members.

* **Transparent**. All formal decision making will be publicly accessible. We
  also strive to share ideas early. This is to enable asynchronous communication
  and collaboration. Only certain categories of sensitive information (e.g.
  financial, and matters concerning particular individuals) will be kept confidential.

* **True to Haskell’s principles**. Haskell's design puts principle ahead of what is
  easy or popular by being firmly guided by the principles of purely functional programming.
  Success, yes, but not at all costs!

# What the Board does
The Board provides the strategic leadership for the Foundation, and is the
decision-making body for everything the Foundation does. More specifically:

* Governance: establishing operating policies, leadership, direction, setting strategy,
  providing guidance
* Staff: appoint and manage senior members of Foundation staff
* Define, curate and track Foundation goals
* Deploy the funds raised by the Foundation to support the Foundation’s goals
* Seek out opportunities to further the goals of the Foundation
* Represent the Haskell community to the world: liaise with sponsors, public bodies
  (ACM, standards committees), etc.
* Ensure success and long-term continuity of the Foundation
* Receive and review financial accounts

# Expectations of Board Members
This section covers what it means to be a member of the Board, and how members aspire
to behave.

* Despite being unpaid, membership on the Board is not an honorary post; it involves
  real work. Members seek to be proactive, not reactive, in developing an inspiring
  vision for the Foundation, and helping to turn that vision into reality. They make
  it a priority to attend meetings, and respond in a timely way to Board discussions.

* Board members are expected to be willing to serve on or chair committees and/or task
  forces that the Board or Executive Team establish.

* Board members strive to act in the best interests of the Foundation and the entire
  Haskell community; they can and should represent the interests of parts of the community
  that they know and understand, but they listen to the views of others, and seek
  collective decisions that balance all those interests.

* Board members abide by Board decisions in public, even if they personally would have
  made a different decision.  That personal view does not have to be secret, but members
  should make it clear that they accept the consensus decision, and will do nothing to
  undermine it.  (If that is not possible, the proper course is to resign from the Board.)

* As prominent members of the Haskell community, Board members aspire to follow the
  Haskell Guidelines for Respectful Communication in all their online communications
  in the Haskell sphere.

* So far as board meetings are concerned, members recognise that (by design) the board is
  diverse in terms of age and experience, and that all have points of view that should be
  valued and shared. Members strive to express their views persuasively but not
  confrontationally, and to contribute to the conversation without dominating it.

# Specific roles within the Board
The Board appoints the following roles from among its membership:
* Chair and Vice Chair
* Treasurer and Vice Treasurer
* Secretary and Vice Secretary

Whenever and as long as the Chair is unable to fulfil their duties the Vice Chair becomes
the acting Chair. Similarly the Vice Treasurer and Vice Secretary.

The primary roles must be held by different individuals, though the "vice" positions may
overlap with the primary ones.

The Board holds an annual election for all of these roles.  If any of them become unavailable
permanently or for an indeterminate period of time, an ad-hoc election can be held. 

Any member of the Board can nominate themselves or others to be elected to any of these roles.

## Chair
The Chair is a servant to the Board and the guardian of its ways of working.  It is the
responsibility of the Chair to execute the operations of the Board. Most notably this includes
scheduling and chairing Board meetings, shepherding the voting process, and carrying out or
delegating action items out of Board resolutions.

## Treasurer
The Treasurer is responsible for overseeing the financial activities of the Foundation. This
includes authorizing tax filings, financial instruments, and transactions, reporting to the
Board on the finances of the Foundation, and proposing budgets and financial policies for the
Board to adopt.

## Secretary
The key role of the Secretary is to be the official source of the history of decisions of the
Board. That is, if we need to know whether some resolution was actually passed, we ask the
Secretary. To fulfil this duty, the Secretary is also responsible for the creation, storage,
and publication of meeting minutes, though it is expected that other members of the Board
help drafting the minutes.

The Secretary is furthermore the guardian of the Board's commitment to transparency.
Accordingly, the Secretary publishes minutes in a timely manner and is responsible for making
periodic announcements to the community of Board activities. The Secretary ensures that
internal communication within the Board is performed in a way that supports our transparency
goals -- for example, that discussions about decision-making are done in a way suitable for
public release.

# Board meetings

We aspire to conduct our business, and especially to make our decisions, in a way that is
inclusive, collaborative, and transparent.  We want everyone’s voice to be heard at meetings.

When there are disagreements we seek to resolve them to everyone’s satisfaction. Taking a
formal vote on a topic where there is serious disagreement may occasionally be necessary,
but should be a rarity.

## Schedule

The Board meets regularly, with Board meetings called by the Chair.

Meetings are scheduled in a manner as equitable as possible to our varied membership. 
Specifically, meetings will not be scheduled in such a way that any one Member is consistently
prevented from participating.

## Agendas
The Chair distributes the meeting agenda 48 hours ahead of time.

Meeting participants may edit the agenda in advance of the meeting to add items or clarifying
questions, but will not remove others' agenda items or otherwise subvert agendas.

The Chair decides on the order of the agenda items, and is generally open for input to this end.

Agenda items that are not being discussed because of time restrictions automatically roll over
to the next meeting's agenda.

After the meeting, the agenda is made public by being added to the [minutes repository](https://gitlab.haskell.org/hf/minutes).

## Minutes
Minutes are taken for all board meetings, and subsequently published in the [minutes repository](https://gitlab.haskell.org/hf/minutes) so that the Haskell
community can see what the Board has been up to.

### Before the meeting

The Secretary creates and distributes the minutes document at the beginning
of the meeting.

### At the meeting
All meeting participants are expected to collaboratively contribute to the meeting minutes.
The Secretary is ultimately responsible for the accurate capturing of the meeting minutes.

### After the meeting
Attendees of the minutes edit the minutes within 24 hours of the meeting to correct any misquotes,
etc., that were recorded.  The Secretary is responsible to review the minutes after those 24
hours to ensure they are appropriate for publishing.

### Ratification and publication
* Minutes are automatically ratified 24 hours after the end of the meeting or as soon as all
  pending objections have been resolved, whichever comes first.

* The Secretary has the authority to redact certain details from the minutes before posting.
  These details might concern, for example, personnel decisions or not-yet-closed fund-raising
  negotiations.

* The Secretary is responsible for publishing the minutes in a timely manner after ratification.
